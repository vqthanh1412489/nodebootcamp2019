const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    require: [true, "User must have username"],
    unique: true
  },
  password: {
    type: String,
    required: [true, "User must have password"]
  },
  age: {
    type: Number,
    required: [true, "User must have age"]
  },
  point: {
    type: Number,
    default: 0
  },
  createAt: {
    type: Date,
    default: Date.now(),
    select: false
  }
});

const User = mongoose.model("users", userSchema);

module.exports = User;
