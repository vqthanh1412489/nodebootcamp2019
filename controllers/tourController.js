const fs = require("fs");
const tours = JSON.parse(
  fs.readFileSync(`${__dirname}/../dev-data/data/tours.json`)
);
exports.getTours = (req, res) => {
  res.status(200).json({
    status: "success",
    timeStamp: req.timeStamp,
    results: tours.length,
    data: {
      tours
    }
  });
};

exports.createTour = (req, res) => {
  const _id = tours[tours.length - 1]._id + 1;
  const tour = Object.assign({ _id }, req.body);
  tours.push(tour);
  fs.writeFile(
    `${__dirname}/../dev-data/data/tours.json`,
    JSON.stringify(tours),
    err => {
      if (err) {
        res.status(400).json({
          success: "fail",
          message: "File not found"
        });
      }
      res.status(201).json({
        success: "success",
        data: {
          tour,
          timeStamp: req.timeStamp
        }
      });
    }
  );
};

exports.getTour = (req, res) => {
  const { id } = req.params;
  const tour = tours.find(ele => ele._id == id);
  if (!tour) {
    return res.status(404).json({
      status: "fail",
      message: "tour not found"
    });
  }
  res.status(200).json({
    status: "success",
    data: {
      tour
    }
  });
};
exports.checkID = (req, res, next, val) => {
  console.log(val);
  next();
};

exports.checkBody = (req, res, next) => {
  if (!req.body.name || !req.body.price) {
    res.status(404).json({
      success: "fail",
      message: "Missing name or price"
    });
  }
  next();
};
