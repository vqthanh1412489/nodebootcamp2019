const User = require("../models/User");
const APIFeature = require("..//utils/APIFeature");

exports.getUsers = async (req, res) => {
  try {
    const feature = new APIFeature(User, req.query)
      .filter()
      .sort()
      .fields()
      .pagination();
    const users = await feature.query;

    res.status(200).json({
      success: "success",
      result: users.length,
      data: users
    });
  } catch (err) {
    res.status(400).json({
      success: "fail",
      message: err
    });
  }
};
exports.getUser = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    res.status(200).json({
      success: "success",
      data: user
    });
  } catch (err) {
    res.status(400).json({
      success: "fail",
      message: "user not found"
    });
  }
};
exports.addUser = async (req, res) => {
  try {
    const user = await User.create(req.body);
    res.status(200).json({
      success: "success",
      data: user
    });
  } catch (err) {
    res.status(400).json({
      success: "fail",
      message: err
    });
  }
};
exports.updateUser = async (req, res) => {
  try {
    const user = await User.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true
    });
    res.status(200).json({
      success: "success",
      data: user
    });
  } catch (err) {
    res.status(400).json({
      success: "fail",
      message: err
    });
  }
};
exports.deleteUser = async (req, res) => {
  try {
    await User.findByIdAndDelete(req.params.id);
    res.status(200).json({
      success: "success",
      data: null
    });
  } catch (err) {
    res.status(400).json({
      success: "fail",
      message: err
    });
  }
};
exports.checkBody = (req, res, next) => {
  const { username, password } = req.body;
  if (!username || !password) {
    res.status(404).json({
      success: "fail",
      message: "username or password is not found"
    });
  }
  next();
};

exports.getUsersStats = async (req, res) => {
  try {
    const stats = await User.aggregate([
      {
        $match: { age: { $gte: 10 } }
      },
      {
        $group: {
          _id: "$password",
          num: { $sum: 1 },
          avgAge: { $avg: "$age" },
          maxAge: { $max: "$age" },
          minAge: { $min: "$age" },
          sumAge: { $sum: "$age" }
        }
      },
      {
        $sort: {
          avgAge: 1 // Inscrease
        }
      }
    ]);
    res.status(200).json({
      success: "success",
      data: stats
    });
  } catch (error) {
    res.status(400).json({
      success: "fail",
      message: error
    });
  }
};
