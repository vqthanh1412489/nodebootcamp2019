const express = require("express");
const userRouters = express.Router();
const userController = require("../controllers/userController");
userRouters
  .route("/")
  .get(userController.getUsers)
  .post(userController.checkBody, userController.addUser);
userRouters.route("/stats").get(userController.getUsersStats);

userRouters
  .route("/:id")
  .get(userController.getUser)
  .put(userController.checkBody, userController.updateUser)
  .delete(userController.deleteUser);
module.exports = userRouters;
