const express = require("express");
const app = express();
const morgan = require("morgan");
const tourRouters = require("./routers/tourRouters");
const userRouters = require("./routers/userRouters");

app.use(express.json());
app.use(morgan("dev"));

// app.use((req, res, next) => {
//   req.ti meStamp = new Date().toISOString();
//   next();
// });

//ROUTER
app.use("/api/v1/tours", tourRouters);
app.use("/api/v1/users", userRouters);

module.exports = app;
